package CucumberFramework.steps;

import utils.DriverFactory;
import utils.ExcelReader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class LoginSteps extends DriverFactory {
	
	// Login

	@Given("^User navigates to Agent Portal$")
	public void user_navigates_to_Agent_Portal() throws Throwable  {

		navigationPage.openAgentPortal();
	}
	
	@And("^User gets an email address from sheetName \"([^\"]*)\" and rowNumber (\\d+)$")
	public void user_gets_an_email_address_from_sheetName_and_rowNumber(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\LoginCredentials.xlsx", sheetName);
		
		String emailAddress = testData.get(rowNumber).get("EmailAddress");
		
		loginPage.getEmailAddress(emailAddress);
	}

	@And("^User gets a password from sheetName \"([^\"]*)\" and rowNumber (\\d+)$")
	public void user_gets_a_password_from_sheetName_and_rowNumber(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\LoginCredentials.xlsx", sheetName);
		
		String password = testData.get(rowNumber).get("PassWord");
		loginPage.getPassword(password);
	}
	
	@And("^User enters an email address$")
	public void user_enters_an_email_address(DataTable emailAddress) throws Throwable {
		
		loginPage.enterEmailAddress(emailAddress, 0, 0);
	}

	@And("^User enters a password$")
	public void user_enters_a_password(DataTable password) throws Throwable {
		
		loginPage.enterPassword(password, 0, 0);
	}
	
	@And("^User clicks on the login button$")
	public void user_clicks_on_the_login_button() throws Throwable {

		loginPage.clickSubmitButton();	
	}

	@When("^User clicks on the login button again$")
	public void user_clicks_on_the_login_button_again() throws Throwable {

		loginPage.clickSubmitButton();
	}

	@Then("^User gets an impersonator from sheetName \"([^\"]*)\" and rowNumber (\\d+)$")
	public void user_gets_an_impersonator_from_sheetName_and_rowNumber(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\LoginCredentials.xlsx", sheetName);
		
		String impersonator = testData.get(rowNumber).get("Impersonator");
		loginPage.getImpersonator(impersonator);
		loginPage.selectImpersonator();
	}

	@Then("^Error message displays \"([^\"]*)\"$")
	public void error_message_displays(String expectedMessage) throws Throwable {

		String actualMessage = loginPage.getErrorMessage();
		Assert.assertEquals(actualMessage, expectedMessage);
	}
		
	@Then("^User should successfully login to the agent portal$")
	public void user_should_successfully_login_to_the_agent_portal() throws Throwable {
	    navigationPage.confirmLogin();
	}
	
	// Forgot Password

	@Then("^The Ideal Agent Logo displays$")
	public void the_Ideal_Agent_Logo_displays() throws Throwable {
		loginPage.verifyLogoDisplays();
	}

	@Then("^The following message displays \"([^\"]*)\"$")
	public void the_following_message_displays(String expectedMessage) throws Throwable {
		
		String actualMessage = loginPage.getLoginHeaderMessage();
		Assert.assertEquals(actualMessage, expectedMessage);
	}

	@Then("^The Forgot Password link displays$")
	public void the_Forgot_Password_link_displays() throws Throwable {
		WebElement forgotPasswordLink = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/form[1]/a[1]"));
		Assert.assertEquals(true, forgotPasswordLink.isDisplayed());
	}

	@Then("^User clicks Forgot Password$")
	public void user_clicks_Forgot_Password() throws Throwable {
		loginPage.clickForgotPassword();
	}

	@Then("^The Request a Reset Link button displays$")
	public void the_Request_a_Reset_Link_button_displays() throws Throwable {
		WebElement requestResetLink = driver.findElement(By.xpath("//button[contains(text(),'Request a Reset Link')]"));
		Assert.assertEquals(true, requestResetLink.isDisplayed());
	}
	
	@When("^User clicks Request a Reset Link button$")
	public void user_clicks_Request_a_Reset_Link_button() throws Throwable {
	    loginPage.clickRequestAResetButton();
	}

	@Then("^A Back to Login link displays$")
	public void a_Back_to_Login_link_displays() throws Throwable {
		WebElement backToLoginLink = driver.findElement(By.xpath("//a[contains(text(),'Back to login')]"));
		Assert.assertEquals(true, backToLoginLink.isDisplayed());
	}

	@Then("^User clicks Back to Login link$")
	public void user_clicks_Back_to_Login_link() throws Throwable {
		loginPage.clickBackToLogin();
	}

	@Then("^Toast notification message displays \"([^\"]*)\"$")
	public void toast_notification_message_displays(String expectedMessage) throws Throwable {
		String actualMessage = loginPage.getToastNotification();
		Assert.assertEquals(actualMessage, expectedMessage);
	}
}
