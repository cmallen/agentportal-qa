package CucumberFramework.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;
import utils.ExcelReader;

import java.util.List;
import java.util.Map;

import org.testng.Assert;

import cucumber.api.DataTable;

public class DashboardSteps extends DriverFactory {

@Given("^user logs into Agent Portal$")
public void user_logs_into_Agent_Portal() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}

@Given("^navigates to the Dashboard$")
public void navigates_to_the_Dashboard() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^referrals is X in the UI$")
public void referrals_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^referrals is X in the BE$")
public void referrals_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^in progress is X in the UI$")
public void in_progress_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^in progress is X in the BE$")
public void in_progress_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^won is X in the UI$")
public void won_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^won is X in the BE$")
public void won_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^lost is X in the UI$")
public void lost_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^lost is X in the BE$")
public void lost_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^active is X in the UI$")
public void active_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^active is X in the BE$")
public void active_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^pending is X in the UI$")
public void pending_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^pending is X in the BE$")
public void pending_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^sold is X in the UI$")
public void sold_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^sold is X in the BE$")
public void sold_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^expired and withdrawn is X in the UI$")
public void expired_and_withdrawn_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^expired and withdrawn is X in the BE$")
public void expired_and_withdrawn_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^off market is X in the UI$")
public void off_market_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^off market is X in the BE$")
public void off_market_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^top converted appointments is X in the UI$")
public void top_converted_appointments_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^top converted appointments is X in the BE$")
public void top_converted_appointments_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^top converted listings is X in the UI$")
public void top_converted_listings_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^top converted listings is X in the BE$")
public void top_converted_listings_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^top converted sales is X in the UI$")
public void top_converted_sales_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^top converted sales is X in the BE$")
public void top_converted_sales_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^agent converted appointments is X in the UI$")
public void agent_converted_appointments_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^agent converted appointments is X in the BE$")
public void agent_converted_appointments_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^agent converted listings is X in the UI$")
public void agent_converted_listings_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^agent converted listings is X in the BE$")
public void agent_converted_listings_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^agent converted sales is X in the UI$")
public void agent_converted_sales_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^agent converted sales is X in the BE$")
public void agent_converted_sales_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^number of new exclusive referrals is X in the UI$")
public void number_of_new_exclusive_referrals_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^number of new exclusive referrals is X in the BE$")
public void number_of_new_exclusive_referrals_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^estimated value of new exclusive referrals is X in the UI$")
public void estimated_value_of_new_exclusive_referrals_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^estimated value of new exclusive referrals is X in the BE$")
public void estimated_value_of_new_exclusive_referrals_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^number of referrals is X in the UI$")
public void number_of_referrals_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^number of referrals is X in the BE$")
public void number_of_referrals_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^number of appointments is X in the UI$")
public void number_of_appointments_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^number of appointments is X in the BE$")
public void number_of_appointments_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^number of listings is X in the UI$")
public void number_of_listings_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^number of listings is X in the BE$")
public void number_of_listings_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^number of closings is X in the UI$")
public void number_of_closings_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^number of closings is X in the BE$")
public void number_of_closings_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^value of referrals is X in the UI$")
public void value_of_referrals_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^value of referrals is X in the BE$")
public void value_of_referrals_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^value of appointments is X in the UI$")
public void value_of_appointments_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^value of appointments is X in the BE$")
public void value_of_appointments_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^value of listings is X in the UI$")
public void value_of_listings_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^value of listings is X in the BE$")
public void value_of_listings_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@When("^value of closings is X in the UI$")
public void value_of_closings_is_X_in_the_UI() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}

@Then("^value of closings is X in the BE$")
public void value_of_closings_is_X_in_the_BE() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     
}
}

