package CucumberFramework.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;
import utils.ExcelReader;

import java.util.List;
import java.util.Map;

import org.testng.Assert;

import cucumber.api.DataTable;

public class PCFSteps extends DriverFactory {
	
	@Given("^Seller is reset for PCF$")
	public void seller_is_reset_for_PCF() throws Throwable {
		// Need update permissions in the DB to reset client
		// Setup JDBC 
	}

	@When("^Enter a \"([^\"]*)\" Seller Concessions$")
	public void enter_a_Seller_Concessions(String SellerConcessions) throws Throwable {
	    pcfPage.getSellerConcessions(SellerConcessions);
	}

	@When("^Enter a \"([^\"]*)\" Contract Price$")
	public void enter_a_Contract_Price(String ContractPrice) throws Throwable {
	    pcfPage.getContractPrice(ContractPrice);
	}
	
	@When("^Select Direct Buyer of Yes$")
	public void select_Direct_Buyer_of_Yes() throws Throwable {
	    pcfPage.clickDirectBuyerDropDown("Yes");
	}
	
	@When("^Select Direct Buyer of No$")
	public void select_Direct_Buyer_of_No() throws Throwable {
		pcfPage.clickDirectBuyerDropDown("No");
	}
	
	@When("^User provides comments$")
	public void user_provides_comments(String comments) throws Throwable {
		pcfPage.enterComments(comments);
	}

	@Then("^Agent clicks Save$")
	public void agent_clicks_Save() throws Throwable {
		formsPage.clickSave();
	}

	@Then("^Form saves successfully and message displays \"([^\"]*)\"$")
	public void form_saves_successfully_and_message_displays(String expectedMessage) throws Throwable {
		
		String actualMessage = formsPage.getSaveMessage();
		Assert.assertEquals(actualMessage, expectedMessage);
	}

	@Given("^Selects Contract Date from sheetName \"([^\"]*)\" and rowNumber (\\d+)$")
	public void selects_Contract_Date_from_sheetName_and_rowNumber(String sheetName, Integer rowNumber) throws Throwable {

		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String yesterday = testData.get(rowNumber).get("Yesterday");
		String currentdate = testData.get(rowNumber).get("CurrentDate");
		String tomorrow = testData.get(rowNumber).get("Tomorrow");
		
		pcfPage.enterYesterdayDate(sheetName, rowNumber);
		pcfPage.enterCurrentDate(sheetName, rowNumber);
		pcfPage.enterTomorrowDate(sheetName, rowNumber);
	}

	@Given("^Selects Closing Date from sheetName \"([^\"]*)\" and rowNumber (\\d+)$")
	public void selects_Closing_Date_from_sheetName_and_rowNumber(String sheetName, Integer rowNumber) throws Throwable {

		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String yesterday = testData.get(rowNumber).get("Yesterday");
		String currentdate = testData.get(rowNumber).get("CurrentDate");
		String tomorrow = testData.get(rowNumber).get("Tomorrow");
		
		pcfPage.enterYesterdayDate(sheetName, rowNumber);
		pcfPage.enterCurrentDate(sheetName, rowNumber);
		pcfPage.enterTomorrowDate(sheetName, rowNumber);
	}


	@Given("^User refreshes form$")
	public void user_refreshes_form() throws Throwable {
	    formsPage.refreshPage();
	}

	@When("^Click Contract Fell Through$")
	public void click_Contract_Fell_Through() throws Throwable {
	    formsPage.clickContractFellThroughPCF();
	}

	@When("^Click Yes$")
	public void click_Yes() throws Throwable {
		formsPage.selectContractFellThroughYes();	
	}
	
	@When("^Click No$")
	public void click_No() throws Throwable {
		formsPage.selectContractFellThroughNo();	
	}

	@Then("^Form will clear$")
	public void form_will_clear() throws Throwable {
		
		pcfPage.verifyPCFCleared();
	}

	@Then("^Form will not clear and will continue to display data$")
	public void form_will_not_clear_and_will_continue_to_display_data() throws Throwable {

		pcfPage.verifyPCFFilled();
	}

}
