package CucumberFramework.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;
import utils.ExcelReader;

import java.util.List;
import java.util.Map;

import org.testng.Assert;

import cucumber.api.DataTable;

public class LAFSteps extends DriverFactory {
	
	@Given("^Seller is reset for ICF$")
	public void seller_is_reset_for_ICF() throws Throwable {
		
		// Need update permissions in the DB to reset client
		// Setup JDBC 
	}
	
	@When("^Agent selects No Lost Listing Option$")
	public void agent_selects_No_Lost_Listing_Option() throws Throwable {
		lafPage.clickOptionDropDown("Dead");
	}
	
	@When("^Agent selects Did Not Reach Option$")
	public void agent_selects_Listing_Appointment_Date_Option() throws Throwable {
		lafPage.clickOptionDropDown("Listing Appointment Date");
	}
	
	@When("^Agent selects Undecided Option$")
	public void agent_selects_Undecided_Option() throws Throwable {
		lafPage.clickOptionDropDown("Undecided");
	}
	
	@When("^Agent selects Soon Option$")
	public void agent_selects_Soon_Option() throws Throwable {
		lafPage.clickOptionDropDown("Soon");
	}
	
	@When("^Agent selects Yes Option$")
	public void agent_selects_Yes_Option() throws Throwable {
		lafPage.clickOptionDropDown("Yes");
	}
	
	@When("^Selects a Reason \"([^\"]*)\" for not setting$")
	public void selects_a_Reason_for_not_setting(DataTable reason) throws Throwable {
		lafPage.clickReasonDropDown(reason, 0, 0);
	}

	@When("^Inputs Initial call comments$")
	public void inputs_Initial_call_comments1(String comments) throws Throwable {
		lafPage.enterComments(comments);
	}

	@When("^Checks No Further Follow Up Required$")
	public void checks_No_Further_Follow_Up_Required() throws Throwable {
		
		lafPage.checkNoFurtherFollowUpRequired();
	}

	@When("^No Further Follow Up Required pop up displays \"([^\"]*)\"$")
	public void no_Further_Follow_Up_Required_pop_up_displays(String expectedMessage) throws Throwable {
		String actualMessage = lafPage.getNoFurtherFollowUpMessage();
		Assert.assertEquals(actualMessage, expectedMessage);
	}

	@When("^User clicks OK and pop Up closes$")
	public void user_clicks_OK_and_pop_Up_closes() throws Throwable {
	    lafPage.closeNoFurtherFollowUpMessage();
	}
	
	@When("^Selects Next follow up date from sheetName \"([^\"]*)\" and rowNumber (\\d+)$")
	public void selects_Next_follow_up_date_from_sheetName_and_rowNumber(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String yesterday = testData.get(rowNumber).get("Yesterday");
		String currentdate = testData.get(rowNumber).get("CurrentDate");
		String tomorrow = testData.get(rowNumber).get("Tomorrow");
		
		lafPage.enterYesterdayDate(sheetName, rowNumber);
		lafPage.enterCurrentDate(sheetName, rowNumber);
		lafPage.enterTomorrowDate(sheetName, rowNumber);
	}

	@When("^Selects List appointment signed date from sheetName \"([^\"]*)\" and rowNumber (\\d+)$")
	public void selects_List_appointment__signed_date_from_sheetName_and_rowNumber(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String yesterday = testData.get(rowNumber).get("Yesterday");
		String currentdate = testData.get(rowNumber).get("CurrentDate");
		String tomorrow = testData.get(rowNumber).get("Tomorrow");
		
		lafPage.enterYesterdayDate(sheetName, rowNumber);
		lafPage.enterCurrentDate(sheetName, rowNumber);
	}

	@Then("^Fails to Submit$")
	public void fails_to_Submit() throws Throwable {
		
		// Need to break down error message per form
	}

	@Then("^Agent clicks Save$")
	public void agent_clicks_Save() throws Throwable {
		formsPage.clickSave();
	}

	@Then("^Form submits successfully$")
	public void form_submits_successfully(String expectedMessage) throws Throwable {

		String actualMessage = formsPage.getSaveMessage();
		Assert.assertEquals(actualMessage, expectedMessage);
	}
	
	@Then("^Form saves successfully and message displays \"([^\"]*)\"$")
	public void form_saves_successfully_and_message_displays(String expectedMessage) throws Throwable {
		String actualMessage = formsPage.getFormSuccessMessage();
		Assert.assertEquals(actualMessage, expectedMessage);
	}

}
