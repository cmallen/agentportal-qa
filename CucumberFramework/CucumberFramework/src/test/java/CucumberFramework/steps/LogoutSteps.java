package CucumberFramework.steps;

import utils.DriverFactory;
import utils.ExcelReader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;

import java.util.List;
import java.util.Map;

import org.testng.Assert;

public class LogoutSteps extends DriverFactory {
	
	@Given("^Toast notification displays \"([^\"]*)\"$")
	public void toast_notification_displays(String expectedMessage) throws Throwable {
		String actualMessage = loginPage.getToastNotification();
		Assert.assertEquals(actualMessage, expectedMessage);
	}
	
	@Then("^User should redirect to the login page$")
	public void user_should_redirect_to_the_login_page() throws Throwable {
		
	    loginPage.verifyLogoDisplays();
	}
	
	@When("^User clicks Log Out$")
	public void user_clicks_Log_Out() throws Throwable {
	    
		navigationPage.navigateToLogOut();
	}

}