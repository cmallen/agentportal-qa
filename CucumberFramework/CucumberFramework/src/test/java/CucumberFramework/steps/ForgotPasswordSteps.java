package CucumberFramework.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;
import utils.ExcelReader;

import java.util.List;
import java.util.Map;

import org.testng.Assert;

import cucumber.api.DataTable;

public class ForgotPasswordSteps extends DriverFactory {


@Given("^User navigates to Agent Portal$")
public void user_navigates_to_Agent_Portal() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@Given("^User enters an email address$")
public void user_enters_an_email_address(DataTable arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
    // E,K,V must be a scalar (String, Integer, Date, enum etc)
 
}

@Given("^User clicks on the login button$")
public void user_clicks_on_the_login_button() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@When("^User clicks Forgot Password$")
public void user_clicks_Forgot_Password() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@When("^Forgot Password page will display$")
public void forgot_Password_page_will_display() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@When("^User will click request a reset link button$")
public void user_will_click_request_a_reset_link_button() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@Then("^Reset Password message will display$")
public void reset_Password_message_will_display() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@Given("^User receives a Reset Password email$")
public void user_receives_a_Reset_Password_email() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@Given("^User opens the Reset Password link$")
public void user_opens_the_Reset_Password_link() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@Given("^Reset Password page displays$")
public void reset_Password_page_displays() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@When("^User enters the Reset Password fields$")
public void user_enters_the_Reset_Password_fields(DataTable arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
    // E,K,V must be a scalar (String, Integer, Date, enum etc)
 
}

@When("^Clicks Reset Password button$")
public void clicks_Reset_Password_button() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@Then("^User should redirect to the login page$")
public void user_should_redirect_to_the_login_page() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@Given("^User enters a password$")
public void user_enters_a_password(DataTable arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
    // E,K,V must be a scalar (String, Integer, Date, enum etc)
 
}

@Given("^User clicks on the login button again$")
public void user_clicks_on_the_login_button_again() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@Given("^User should successfully login to the agent portal$")
public void user_should_successfully_login_to_the_agent_portal() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}

@When("^User clicks Log Out$")
public void user_clicks_Log_Out() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
 
}
}