Feature: Dashboard
		Agent Stats (Top Section)
		Converted (Middle Section)
		Pipeline (Bottom Section)

Background: Login and navigate to the Dashboard
	Given user logs into Agent Portal  
	And navigates to the Dashboard

@Client
Scenario: Dashboard - Agent Stats - Referrals
	When referrals is X in the UI
	Then referrals is X in the BE

@Client
Scenario: Dashboard - Agent Stats - In Progress
	When in progress is X in the UI
	Then in progress is X in the BE

@Client
Scenario: Dashboard - Agent Stats - Won
	When won is X in the UI
	Then won is X in the BE

@Client	
Scenario: Dashboard - Agent Stats - Lost
	When lost is X in the UI
	Then lost is X in the BE

@Client
Scenario: Dashboard - Agent Stats - Active
	When active is X in the UI
	Then active is X in the BE
	
@Client
Scenario: Dashboard - Agent Stats - Pending
	When pending is X in the UI
	Then pending is X in the BE

@Client
Scenario: Dashboard - Agent Stats - Sold
	When sold is X in the UI
	Then sold is X in the BE

@Client
Scenario: Dashboard - Agent Stats - Expired/Withdrawn
	When expired and withdrawn is X in the UI
	Then expired and withdrawn is X in the BE

@Client	
Scenario: Dashboard - Agent Stats - Off Market
	When off market is X in the UI
	Then off market is X in the BE
	
@Converted
Scenario: Dashboard - Top - Converted Appointments
	When top converted appointments is X in the UI
	Then top converted appointments is X in the BE

@Converted	
Scenario: Dashboard - Top - Converted Listings
	When top converted listings is X in the UI
	Then top converted listings is X in the BE

@Converted	
Scenario: Dashboard - Top - Converted Sales
	When top converted sales is X in the UI
	Then top converted sales is X in the BE

@Converted
Scenario: Dashboard - Agent - Converted Appointments
	When agent converted appointments is X in the UI
	Then agent converted appointments is X in the BE

@Converted
Scenario: Dashboard - Agent - Converted Listings
	When agent converted listings is X in the UI
	Then agent converted listings is X in the BE

@Converted	
Scenario: Dashboard - Agent - Converted Sales
	When agent converted sales is X in the UI
	Then agent converted sales is X in the BE

@Pipeline
Scenario: Dashboard - Pipeline - Number of New Exclusive Referrals
	When number of new exclusive referrals is X in the UI
	Then number of new exclusive referrals is X in the BE
	
@Pipeline
Scenario: Dashboard - Pipeline - Estimated Value of New Exclusive Referrals
	When estimated value of new exclusive referrals is X in the UI
	Then estimated value of new exclusive referrals is X in the BE

@Pipeline	
Scenario: Dashboard - Pipeline - Number of Referrals
	When number of referrals is X in the UI
	Then number of referrals is X in the BE

@Pipeline	
Scenario: Dashboard - Pipeline - Number of Appointments
	When number of appointments is X in the UI
	Then number of appointments is X in the BE

@Pipeline	
Scenario: Dashboard - Pipeline - Number of Listings
	When number of listings is X in the UI
	Then number of listings is X in the BE

@Pipeline	
Scenario: Dashboard - Pipeline - Number of Closings
	When number of closings is X in the UI
	Then number of closings is X in the BE

@Pipeline	
Scenario: Dashboard - Pipeline - Value of Referrals
	When value of referrals is X in the UI
	Then value of referrals is X in the BE

@Pipeline	
Scenario: Dashboard - Pipeline - Value of Appointments
	When value of appointments is X in the UI
	Then value of appointments is X in the BE

@Pipeline	
Scenario: Dashboard - Pipeline - Value of Listings
	When value of listings is X in the UI
	Then value of listings is X in the BE
	
@Pipeline
Scenario: Dashboard - Pipeline - Value of Closings
	When value of closings is X in the UI
	Then value of closings is X in the BE
