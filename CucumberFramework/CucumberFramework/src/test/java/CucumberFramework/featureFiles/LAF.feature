Feature: Seller - Listing Appointment Form
		Submit LAF
		
Background:
	Given User navigates and logs into Agent Portal 
	And Seller is reset for LAF
	And Select seller 
	
@NoLostListing
Scenario Outline: Submit LAF as No Lost Listing
	When Agent selects No Lost Listing Option
	And Selects a Reason "<Reason>" for not securing listing
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|
	|Not Selling|
	|Went with Another Agent|
	|Poor Condition or Unrealistic Pricing|
	|FSBO|
	|Other|
	
@NoLostListing
Scenario Outline: Submit LAF as No Lost Listing with No Further Follow Up
	When Agent selects No Lost Listing Option
	And Selects a Reason "<Reason>" for not securing listing
	And Checks No Further Follow Up Required
	And No Further Follow Up Required pop up displays "Selecting “No Further Follow Up Required” removes this referral from your pipeline entirely. If you intend to follow up at a later date, uncheck the “No Further Follow Up Required” box and set a date in the future to follow up. You can also use the Assist Button if you need our assistance with this referral."
	And User clicks OK and pop Up closes
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|
	|Not Selling|
	|Went with Another Agent|
	|Poor Condition or Unrealistic Pricing|
	|FSBO|
	|Other|
	
@ListingAppointmentCancelled
Scenario Outline: Submit LAF as Listing Appointment Cancelled
	When Agent selects Listing Appointment Cancelled Option
	And Selects a Reason "<Reason>" for not securing listing
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|
	|Scheduled|
	|Rescheduled|
	
@Undecided
Scenario Outline: Submit LAF as Undecided with Past Date
	When Agent selects Undecided Option
	And Selects a Reason "<Reason>" for not securing listing
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Fails to Submit 
	
	Examples:
	|Reason|SheetName|RowNumber|
	|Interviewing other agents|yesterday|0|
	|Trying For Sale By Owner|yesterday|0|
	|Currently Listed - Waiting on Exp or Release|yesterday|0|
	
@Undecided
Scenario Outline: Submit LAF as Undecided with Current Date
	When Agent selects Undecided Option
	And Selects a Reason "<Reason>" for not securing listing
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|SheetName|RowNumber|
	|Interviewing other agents|currentdate|0|
	|Trying For Sale By Owner|currentdate|0|
	|Currently Listed - Waiting on Exp or Release|currentdate|0|
	
@Undecided
Scenario Outline: Submit LAF as Undecided with Future Date
	When Agent selects Undecided Option
	And Selects a Reason "<Reason>" for not securing listing
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|SheetName|RowNumber|
	|Interviewing other agents|tomorrow|0|
	|Trying For Sale By Owner|tomorrow|0|
	|Currently Listed - Waiting on Exp or Release|tomorrow|0|
	
@Soon
Scenario Outline: Submit LAF as Soon with Past Date
	When Agent selects Soon Option
	And Selects a Reason "<Reason>" for not securing listing
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Fails to Submit 
	
	Examples:
	|Reason|SheetName|RowNumber|
	|House preparation or repairs|yesterday|0|
	|Seller has listing agreement|yesterday|0|
	|Waiting until later in the year|yesterday|0|

@Soon
Scenario Outline: Submit LAF as Soon with Current Date
	When Agent selects Soon Option
	And Selects a Reason "<Reason>" for not securing listing
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|SheetName|RowNumber|
	|House preparation or repairs|currentdate|0|
	|Seller has listing agreement|currentdate|0|
	|Waiting until later in the year|currentdate|0|
	
@Soon
Scenario Outline: Submit LAF as Soon with Future Date
	When Agent selects Soon Option
	And Selects a Reason "<Reason>" for not securing listing
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Please provide details about the listing appointment
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|SheetName|RowNumber|
	|House preparation or repairs|tomorrow|0|
	|Seller has listing agreement|tomorrow|0|
	|Waiting until later in the year|tomorrow|0|
	
@Yes
Scenario Outline: Submit LAF as Yes with Past Date
	When Agent selects Yes Option
	And Selects Listing Agreement Signed Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Active Listing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<ListPrice>" List Price 
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|ListPrice|
	|yesterday|0|yesterday|0|123456|
	
@Yes
Scenario Outline: Submit LAF as Yes with Current Date
	When Agent selects Yes Option
	And Selects Listing Agreement Signed Date from sheetName "<SheetName>" and rowNumber <RowNumber> 
	And Selects Active Listing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<ListPrice>" List Price 
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|ListPrice|
	|currentdate|0|currentdate|0|123456|
	
@Yes
Scenario Outline: Submit LAF as Yes with Future Date
	When Agent selects Yes Option
	And Selects Listing Agreement Signed Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Active Listing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<ListPrice>" List Price 
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|ListPrice|
	|tomorrow|0|tomorrow|0|123456|