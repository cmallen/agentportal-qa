@login
Feature: Login to the portal (agent, impersonator, valid, invalid, error messages)

Scenario Outline: Login with valid credentials - impersonator
	Given User navigates to Agent Portal 
	And User gets an email address from sheetName "<SheetName>" and rowNumber <RowNumber> 
	And User clicks on the login button 
	And User gets a password from sheetName "<SheetName>" and rowNumber <RowNumber>
	When User clicks on the login button again
	And User gets an impersonator from sheetName "<SheetName>" and rowNumber <RowNumber> 
	Then User should successfully login to the agent portal
	
		Examples:
		|SheetName|RowNumber|
		|impersonator|0|

Scenario Outline: Login with valid credentials - agent (uppercase and lowercase)
	Given User navigates to Agent Portal 
	And User gets an email address from sheetName "<SheetName>" and rowNumber <RowNumber>
	And User clicks on the login button 
	And User gets a password from sheetName "<SheetName>" and rowNumber <RowNumber> 
	When User clicks on the login button again
	Then User should successfully login to the agent portal
	
		Examples:
		|SheetName|RowNumber|
		|agent|0|
		|agent|1|
	
Scenario Outline: Login with invalid credentials - email
	Given User navigates to Agent Portal 
	And User gets an email address from sheetName "<SheetName>" and rowNumber <RowNumber>   
	When User clicks on the login button 
	Then Error message displays "This email is not registered with Ideal Agent"
	
		Examples:
		|SheetName|RowNumber|
		|invalid|0|
		
Scenario Outline: Login with invalid credentials - password
	Given User navigates to Agent Portal 
	And User gets an email address from sheetName "<SheetName>" and rowNumber <RowNumber> 
	And User clicks on the login button 
	And User gets a password from sheetName "<SheetName>" and rowNumber <RowNumber> 
	When User clicks on the login button again
	Then Error message displays "Wrong password. Please try again"
		
		Examples:
		|SheetName|RowNumber|
		|invalid|1|
		
Scenario: Validate logos and messages on Login Page
	Given User navigates to Agent Portal 
	Then The Ideal Agent Logo displays
	And The following message displays "This application is for authorized partner agents of Ideal Agent only"

Scenario: Validate Forgot Password (Screen)
	Given User navigates to Agent Portal 
	Then The Forgot Password link displays
	And User clicks Forgot Password
	And The following message displays "This application is for authorized partner agents of Ideal Agent only"
	And The following message displays "Click the “Request a Reset Link” to be sent a link to reset your password to your email."
	And The Request a Reset Link button displays
	And A Back to Login link displays

	
Scenario: Validate Forgot Password (Buttons)
	Given User navigates to Agent Portal 
	And User clicks Forgot Password
	When User clicks Request a Reset Link button
	Then Toast notification message displays "The link has been sent! Check your e-mail."
	
	
