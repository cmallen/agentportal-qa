Feature: Seller - Purchace Contract Form
		Submit PCF as Direct Buyer Yes
		Submit PCF as Direct Buyer No
		PCF Contract Fell Through
		
Background:
	Given User navigates and logs into Agent Portal 
	And Seller is reset for PCF
	And Select seller 
	
Scenario Outline: Submit PCF as Yes with Direct Buyer
	When Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of Yes
	And User provides comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|tomorrow|0|tomorrow|0|123456|123456|
	
Scenario Outline: Submit PCF as Yes without Direct Buyer
	When Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of No
	And User provides comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|tomorrow|0|tomorrow|0|123456|123456|
	
Scenario Outline: Submit PCF as Yes with Past Date
	When Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of Yes
	And User provides comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|yesterday|0|yesterday|0|123456|123456|
	
Scenario Outline: Submit PCF as Yes with Current Date
	When Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of Yes
	And User provides comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|currentdate|0|currentdate|0|123456|123456|
	
Scenario Outline: Submit PCF as Yes with Future Date
	When Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of Yes
	And User provides comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|tomorrow|0|tomorrow|0|123456|123456|
	
Scenario Outline: Submit PCF as Yes with Future Date
	When Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of Yes
		And User provides comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|tomorrow|0|tomorrow|0|123456|123456|
	
Scenario Outline: Submit PCF Contract Fell Through - Yes
	And Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of Yes
		And User provides comments
	And Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	And User refreshes form
	When Click Contract Fell Through
	And Click Yes
	Then Form will clear
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|tomorrow|0|tomorrow|0|123456|123456|
	
Scenario Outline: Submit PCF Contract Fell Through - No
	And Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of Yes
	And User provides comments
	And Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	And User refreshes form
	When Click Contract Fell Through
	And Click No
	Then Form will not clear and will continue to display data
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|tomorrow|0|tomorrow|0|123456|123456|