Feature: Seller - Purchace Contract Form
		Submit SPF 
		Update PCF
		Contract Fell Through
		
Background:
	Given User navigates and logs into Agent Portal 
	And Seller is reset for SPF Under Contract
	And Select seller 
		
Scenario Outline: Update PCF
	When Agent selects No Option
	And Selects Contract Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Selects Closing Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Enter a "<SellerConcessions>" Seller Concessions
	And Enter a "<ContractPrice>" Contract Price
	And Select Direct Buyer of Yes
	And User provides comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|SheetName|RowNumber|SellerConcessions|ContractPrice|
	|currentday|0|currentday|0|123456|654321|
	
Scenario Outline: Submit SPF with Current Date
	When Agent selects Yes Option
	And Upload and ALTA Form
	And Selects Final Sold Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|currentday|0|
	
Scenario Outline: Submit SPF with Past Date
	When Agent selects Yes Option
	And Upload and ALTA Form
	And Selects Final Sold Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|yesterday|0|
	
Scenario Outline: Submit SPF with Future Date
	When Agent selects Yes Option
	And Upload and ALTA Form
	And Selects Final Sold Date from sheetName "<SheetName>" and rowNumber <RowNumber>
	Then Agent clicks Save
	And Form Fails to Submit
	
	Examples:
	|SheetName|RowNumber|
	|tomorrow|0|
	
Scenario: Contract Fell Through
	When Agent selects Yes Option
	And Contract Fell Through
	And Clicks Yes
	Then PCF will clear