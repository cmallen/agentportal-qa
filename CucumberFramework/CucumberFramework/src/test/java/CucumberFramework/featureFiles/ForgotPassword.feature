@ForgotPassword
Feature: Forgot Password 
			Forgot Password with valid credentials
			Reset Password with email link
			Login with new password
		
Scenario: Forgot Password with valid credentials
	Given User navigates to Agent Portal
	And User enters an email address 
		| IdealAgentQA@gmail.com | 
	And User clicks on the login button 
	When User clicks Forgot Password
	And Forgot Password page will display 
	When User will click request a reset link button
	Then Reset Password message will display 
	
Scenario: Reset Password with email link
	Given User receives a Reset Password email
	And User opens the Reset Password link
	And Reset Password page displays
	When User enters the Reset Password fields
		| Password2! |
	And Clicks Reset Password button
	Then User should redirect to the login page
	
Scenario: Login with new password
	Given User navigates to Agent Portal
	And User enters an email address
		| IdealAgentQA@gmail.com |   
	And User clicks on the login button 
	And User enters a password
		| Password2! | 
	And User clicks on the login button again
	And User should successfully login to the agent portal
	When User clicks Log Out
	Then User should redirect to the login page	
		
