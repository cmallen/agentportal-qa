Feature: Seller - Initial Contract Form 
		Submit ICF as Dead
		Submit ICF as Did Not Reach
		Submit ICF as Not Set Yet
		Submit ICF as Yes
		Submit ICF as Yes Virtual
		
Background:
	Given User navigates and logs into Agent Portal 
	And Seller is reset for ICF
	And Select seller 
		
@Dead
Scenario Outline: Submit ICF as Dead - Reasons
	When Agent selects Dead Option
	And Selects a Reason "<Reason>" for not setting
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|
	|Not Selling|
	|Went with Another Agent|
	|Property Sold|
	|iBuyer Platform|
	|Pricing Expectations are unrealistic|
	|Other|
	
@Dead
Scenario Outline: Submit ICF as Dead with No Further Follow Up
	When Agent selects Dead Option
	And Selects a Reason "<Reason>" for not setting
	And Checks No Further Follow Up Required
	And No Further Follow Up Required pop up displays "Selecting “No Further Follow Up Required” removes this referral from your pipeline entirely. If you intend to follow up at a later date, uncheck the “No Further Follow Up Required” box and set a date in the future to follow up. You can also use the Assist Button if you need our assistance with this referral."
	And User clicks OK and pop Up closes
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|
	|Not Selling|
	|Went with Another Agent|
	|Property Sold|
	|iBuyer Platform|
	|Pricing Expectations are unrealistic|
	|Other|
	
@DidNotReach
Scenario Outline: Submit ICF as Did Not Reach with Past Date
	When Agent selects Did Not Reach Option
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Fails to Submit 
	
	Examples:
	|SheetName|RowNumber|
	|yesterday|0|

@DidNotReach
Scenario Outline: Submit ICF as Did Not Reach with Present Date
	When Agent selects Did Not Reach Option 
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|currentdate|0|

@DidNotReach
Scenario Outline: Submit ICF as Did Not Reach with Future Date
	When Agent selects Did Not Reach Option
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|tomorrow|0|
	
@NotSetYet
Scenario Outline: Submit ICF as Not Set Yet - Reasons
	When Agent selects Not Set Yet Option
	And Selects a Reason "<Reason>" for not setting
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Fails to Submit
	
	Examples:
	|SheetName|RowNumber|Reason|
	|currentdate|0|Not Ready (repairs, paints, etc)|
	|currentdate|0|Not Decision Maker|
	|currentdate|0|Currently Listed|
	|currentdate|0|For Sale By Owner|
	|currentdate|0|Other|

@NotSetYet
Scenario Outline: Submit ICF as Not Set Yet with Past Date
	When Agent selects Not Set Yet Option
	And Selects a Reason "<Reason>" for not setting
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Fails to Submit
	
	Examples:
	|Reason|SheetName|RowNumber|
	|For Sale By Owner|yesterday|0|
	
@NotSetYet
Scenario Outline: Submit ICF as Not Set Yet with Present Date
	When Agent selects Not Set Yet Option
	And Selects a Reason "<Reason>" for not setting
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|SheetName|RowNumber|
	|For Sale By Owner|currentdate|0|

@NotSetYet	
Scenario Outline: Submit ICF as Not Set Yet with Future Date
	When Agent selects Not Set Yet Option
	And Selects a Reason "<Reason>" for not setting
	And Selects Next follow up date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|Reason|SheetName|RowNumber|
	|For Sale By Owner|tomorrow|0|
	
@Yes
Scenario Outline: Submit ICF as Yes with Past Date
	When Agent selects Yes Option
	And Selects List appointment date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|yesterday|0|
	
@Yes
Scenario Outline: Submit ICF as Yes with Present Date
	When Agent selects Yes Option
	And Selects List appointment date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|currentdate|0|

@Yes
Scenario Outline: Submit ICF as Yes with Future Date
	When Agent selects Yes Option
	And Selects List appointment date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|tomorrow|0|

@YesVirtual
Scenario Outline: Submit ICF as Yes Virtual with Past Date
	When Agent selects Yes Virtual Option
	And Selects List appointment date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|yesterday|0|
	
@YesVirtual
Scenario Outline: Submit ICF as Yes Virtual with Present Date
	When Agent selects Yes Virtual Option
	And Selects List appointment date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|currentdate|0|

@YesVirtual	
Scenario Outline: Submit ICF as Yes Virtual with Future Date
	When Agent selects Yes Virtual Option
	And Selects List appointment date from sheetName "<SheetName>" and rowNumber <RowNumber>
	And Inputs Initial call comments
	Then Agent clicks Save
	And Form saves successfully and message displays "Form successfully updated!"
	
	Examples:
	|SheetName|RowNumber|
	|tomorrow|0|