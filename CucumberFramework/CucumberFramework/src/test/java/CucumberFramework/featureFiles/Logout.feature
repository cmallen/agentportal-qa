Feature: Login to the portal
		Login to the portal (agent, impersonator, valid, invalid, error messages)
		Forgot Password 
		Logout of the portal
		
Scenario Outline: Logout of Agent Portal
	Given User navigates to Agent Portal
	And User gets an email address from sheetName "<SheetName>" and rowNumber <RowNumber> 
	And User clicks on the login button 
	And User gets a password from sheetName "<SheetName>" and rowNumber <RowNumber>
	And User clicks on the login button again
	And User should successfully login to the agent portal
	And Toast notification displays "Login successful!"
	When User clicks Log Out
	Then User should redirect to the login page
	
	Examples:
	|SheetName|RowNumber|
	|agent|0|