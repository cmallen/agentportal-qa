package CucumberFramework.runner;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions (
		features = "src/test/java/CucumberFramework/featureFiles/Login.feature",
		glue = {"steps"},
		plugin = { 
				"pretty", 
				"json:target/cucumber-reports/Cucumber.json",
				"junit:target/cucumber-reports/Cucumber.xml",
				"html:target/cucumber-reports"}
		)

public class LoginRunner {


}


