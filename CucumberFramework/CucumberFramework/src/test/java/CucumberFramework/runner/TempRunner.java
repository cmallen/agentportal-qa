package CucumberFramework.runner;

import java.io.File;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import utils.FileReaderManager;

@RunWith(Cucumber.class)

@CucumberOptions(
			features = "src/test/java/CucumberFramework/featureFiles",
			glue = { "CucumberFramework.steps" }, 
			monochrome = true, 
			tags = {"@template"}, 
			plugin = { 				"pretty", 
					"json:target/cucumber-reports/Cucumber.json",
					"junit:target/cucumber-reports/Cucumber.xml",
					"html:target/cucumber-reports",
					"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"})

public class TempRunner extends AbstractTestNGCucumberTests {
	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File(FileReaderManager.getInstance().getConfigReader().getReportConfigPath()));
	}
}