package utils;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import pageObjects.LoginPage;
import pageObjects.DashboardPage;
import pageObjects.FormsPage;
import pageObjects.ICFPage;
import pageObjects.LAFPage;
import pageObjects.NavigationPage;
import pageObjects.PCFPage;
import pageObjects.SPFPage;
import pageObjects.SettingsPage;

public class DriverFactory {
	public static WebDriver driver;
	
	// pageObject classes
	public static LoginPage loginPage;
	public static DashboardPage dashboardPage;
	public static SettingsPage settingsPage;
	public static NavigationPage navigationPage;
	public static FormsPage formsPage;
	public static ICFPage icfPage;
	public static LAFPage lafPage;
	public static PCFPage pcfPage;
	public static SPFPage spfPage;

	public WebDriver getDriver() {
		try {
			// Read Config
			ReadConfigFile file = new ReadConfigFile();
			String browserName = file.getBrowser();

			switch (browserName) {

			case "firefox":
				// code
				if (null == driver) {
					System.setProperty("webdriver.gecko.driver", Constant.GECKO_DRIVER_DIRECTORY);
					DesiredCapabilities capabilities = DesiredCapabilities.firefox();
					capabilities.setCapability("marionette", true);
					driver = new FirefoxDriver();
				}
				break;

			case "chrome":
				// code
				if (null == driver) {
					System.setProperty("webdriver.chrome.driver", Constant.CHROME_DRIVER_DIRECTORY);
					// CHROME OPTIONS
					driver = new ChromeDriver();
					driver.manage().window().maximize();
				}
				break;

			}
			
		} catch (Exception e) {
			
			System.out.println("Unable to load browser: " + e.getMessage());
			
		} finally {
			
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			
			// initilize pageObject class
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			dashboardPage = PageFactory.initElements(driver, DashboardPage.class);
			settingsPage = PageFactory.initElements(driver, SettingsPage.class);
			navigationPage = PageFactory.initElements(driver, NavigationPage.class);
			formsPage = PageFactory.initElements(driver, FormsPage.class);
			icfPage = PageFactory.initElements(driver, ICFPage.class);
			lafPage = PageFactory.initElements(driver, LAFPage.class);
			pcfPage = PageFactory.initElements(driver, PCFPage.class);
			spfPage = PageFactory.initElements(driver, SPFPage.class);
			
		}
		
		return driver;
	}
}
