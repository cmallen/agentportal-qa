package utils;

public class FileReaderManager {

    private static FileReaderManager fileReaderManager = new FileReaderManager();
    private static ReadConfigFile readConfigFile;

    private FileReaderManager() {
    }

    public static FileReaderManager getInstance( ) {
        return fileReaderManager;
    }

    public ReadConfigFile getConfigReader() {
        return (readConfigFile == null) ? new ReadConfigFile() : readConfigFile;
    }

}