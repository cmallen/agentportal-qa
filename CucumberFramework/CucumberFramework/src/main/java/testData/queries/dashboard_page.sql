/* kelly parks = '2133084000001945001' */
/* chad semans = '2133084000036996188' */
set @agent = '2133084000001945001'; 

set @ref = (select count(*) from portal_idealagent.lead where exchange_date is not null and inactive = '0' and assigned_agent = @agent);
-- set @inprogress = (); 
-- set @won = (); 
set @lost = (select count(*) from portal_idealagent.lead where exchange_date is not null and inactive = '0' and exchange_status in ('Buyer NO', 'NO', 'NO - Listed w/NON-IDEAL AGENT', 'NO - SOLD') and assigned_agent = @agent);
set @activ = (select count(*) from portal_idealagent.lead where exchange_date is not null and inactive = '0' and exchange_status in ('ACTIVE') and assigned_agent = @agent); -- wrong
-- set @pending = ();
set @expired = (select count(*) from portal_idealagent.lead where exchange_date is not null and inactive = '0' and exchange_status in ('EXPIRED','WITHDRAWN') and assigned_agent = @agent); -- wrong
set @offmarket = (select count(*) from portal_idealagent.lead where exchange_date is not null and inactive = '0' and property_status in ('OFF MARKET') and assigned_agent = @agent); -- wrong 
-- set @closing = ();

/* Converted */
set @appts = (select count(*) from portal_idealagent.lead where listing_appt_date is not null and inactive = '0' and assigned_agent = @agent);
set @listing = (select count(*) from portal_idealagent.lead where active_listing_date is not null and inactive = '0' and assigned_agent = @agent);
set @sold = (select count(*) from portal_idealagent.lead where sold_date is not null and inactive = '0' and assigned_agent = @agent);

/* Pipeline */
set @30daynewref = (select count(est_list_price) from portal_idealagent.lead where exchange_date is not null and inactive = '0' and assigned_agent = @agent and DATE_SUB(CURDATE(), INTERVAL 30 DAY) <= date (exchange_date));
set @30dayestval = (select sum(est_list_price) from portal_idealagent.lead where exchange_date is not null and inactive = '0' and assigned_agent = @agent and DATE_SUB(CURDATE(), INTERVAL 30 DAY) <= date (exchange_date));

set @refsum = (select sum(est_list_price) from portal_idealagent.lead where exchange_date is not null and inactive = '0' and assigned_agent = @agent);
set @apptssum = (select sum(est_list_price) from portal_idealagent.lead where listing_appt_date is not null and inactive = '0' and assigned_agent = @agent);
set @listingsum = (select sum(list_price) from portal_idealagent.lead where active_listing_date is not null and inactive = '0' and assigned_agent = @agent);
set @soldsum = (select sum(sold_price) from portal_idealagent.lead where sold_date is not null and inactive = '0' and assigned_agent = @agent);
-- set @closingsum = ();

select distinct
/* Top */
(round(@ref, 0)) as 'Referrals',
(round(@appts, 0)) as 'In Progress',
(round(@won, 0)) as 'Won',
(round(@lost, 0)) as 'Lost',
(round(@activ, 0)) as 'Active',
(round(@pending, 0)) as 'Pending',
(round(@sold, 0)) as 'Sold',
(round(@expired, 0)) as 'Expired / Withdrawn',
(round(@offmarket, 0)) as 'Off Market',

/* Converted */
(round((@appts / @ref) * 100, 1)) as 'Converted Appointments',
(round((@listing / @appts) * 100, 1)) as 'Converted Listings',
(round((@sold / @listing) * 100, 1)) as 'Converted Sales',

/* Pipeline */
(round(@30daynewref, 0)) as '30 Day Referrals',
(round(@30dayestval, 0)) as '30 Day Est Value',
(round(@ref, 0)) as 'Referrals',
(round(@refsum, 0)) as 'Referrals Totals',
(round(@appts, 0)) as 'Appointments',
(round(@apptssum, 0)) as 'Appointments Totals',
(round(@listing, 0)) as 'Listings',
(round(@listingsum, 0)) as 'Listing Totals',
(round(@closing, 0)) as 'Closings',
(round(@closingsum, 0)) as 'Closing Totals'
from portal_idealagent.lead;









