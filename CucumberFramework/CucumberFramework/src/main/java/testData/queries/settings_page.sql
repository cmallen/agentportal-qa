/* chad */
set @userid = '22605';

/* ALL */
select id, 
Brokerage, 
email, phone, 
mailing_country, mailing_state, mailing_city, mailing_street, mailing_zip, 
assistant, assistant_email, assistant_phone
from portal_idealagent.agent
where id = @userid;

/* Personal Information */
select id, Brokerage
from portal_idealagent.agent
where id = @userid;

/* Contact Information */
select id, email, phone
from portal_idealagent.agent
where id = @userid;

/* Address Information */
select id, mailing_country, mailing_state, mailing_city, mailing_street, mailing_zip
from portal_idealagent.agent
where id = @userid;

/* Assistant Information */
select id, assistant, assistant_email, assistant_phone
from portal_idealagent.agent
where id = @userid;