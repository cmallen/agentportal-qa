package pageObjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FormsPage extends BasePage {
	
	public @FindBy(xpath = "") WebElement select_Seller;
	public @FindBy(xpath = "") WebElement select_Buyer;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[7]/button[1]")
	WebElement saveButton;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[1]")
	WebElement saveMessage;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[1]")
	WebElement 	formSuccessMessage;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[6]/div[1]/button[1]")
	WebElement pcfContractFellThroughButton;
	
	public @FindBy(xpath = "")
	WebElement spfContractFellThroughButton;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[6]/div[1]/div[1]/div[1]/button[2]	")
	WebElement contractFellThroughNo;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[2]/div[1]/form[1]/div[6]/div[1]/div[1]/div[1]/button[1]")
	WebElement contractFellThroughYes;
	
	public FormsPage() throws IOException {
		super();
	}
	
	public FormsPage selectSeller() throws Exception {
		
		waitAndClickElement(select_Seller);
		return new FormsPage();	
	}
	
	public FormsPage selectBuyer() throws Exception {
		
		waitAndClickElement(select_Buyer);
		return new FormsPage();	
	}
	
	public FormsPage clickSave() throws Exception	{
		waitAndClickElement(saveButton);
		return new FormsPage();	
	}

	public String getSaveMessage()	{
		return driver.findElement((By) saveMessage).getText();
	}
	
	public String getFormSuccessMessage() { 
		  return driver.findElement((By) formSuccessMessage).getText();
		}
	
	public FormsPage refreshPage() throws Exception {
		
		driver.navigate().refresh();
		return new FormsPage();	
		
		}
	
	public FormsPage clickContractFellThroughPCF() throws Exception {
		
		waitAndClickElement(pcfContractFellThroughButton);
		return new FormsPage();	
	}
	
	public FormsPage clickContractFellThroughSPF() throws Exception {
		
		waitAndClickElement(spfContractFellThroughButton);
		return new FormsPage();	
	}
	
	public FormsPage selectContractFellThroughYes() throws Exception {
		
		waitAndClickElement(contractFellThroughYes);
		return new FormsPage();	
	}
	
	public FormsPage selectContractFellThroughNo() throws Exception {
		
		waitAndClickElement(contractFellThroughNo);
		return new FormsPage();	
	}
	
}
