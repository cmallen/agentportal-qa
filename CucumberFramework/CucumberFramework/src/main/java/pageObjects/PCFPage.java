package pageObjects;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.DataTable;
import utils.ExcelReader;

public class PCFPage extends BasePage {
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/span[1]/input[1]")
	WebElement contractDate;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/span[1]/input[1]")
	WebElement closingDate;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/input[1]")
	WebElement sellerConcessions;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/input[1]")
	WebElement contractPrice;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/form[1]/div[4]/div[1]/select[1]")
	WebElement directBuyer;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/form[1]/div[5]/div[1]/textarea[1]")
	WebElement comments;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[5]/button[1]")
	WebElement saveButton;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[1]")
	WebElement saveMessage;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[1]")
	WebElement 	formSuccessMessage;
	
	public PCFPage() throws IOException {
		super();
	}
	
	public PCFPage enterYesterdayDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String yesterday = testData.get(rowNumber).get("Yesterday");
		
		pcfPage.getContractDate(yesterday);
		pcfPage.getClosingDate(yesterday);
		
		Assert.assertNotNull(yesterday);
	
		return new PCFPage();
	}
	
	public PCFPage enterCurrentDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String currentdate = testData.get(rowNumber).get("CurrentDate");
		
		pcfPage.getContractDate(currentdate);
		pcfPage.getClosingDate(currentdate);
		
		Assert.assertNotNull(currentdate);
		
		return new PCFPage();
	}
	
	public PCFPage enterTomorrowDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String tomorrow = testData.get(rowNumber).get("Tomorrow");
		
		pcfPage.getContractDate(tomorrow);
		pcfPage.getClosingDate(tomorrow);
		
		Assert.assertNotNull(tomorrow);
		
		return new PCFPage();
	}
	
	public void getContractDate(String date) throws Throwable  	{
		sendKeysToWebElement(contractDate, date);
	}
	
	public void getClosingDate(String date) throws Throwable  	{
		sendKeysToWebElement(closingDate, date);
	}
	
	public void getSellerConcessions(String date) throws Throwable  	{
		sendKeysToWebElement(sellerConcessions, date);
	}
	
	public void getContractPrice(String date) throws Throwable  	{
		sendKeysToWebElement(contractPrice, date);
	}

	public PCFPage clickDirectBuyerDropDown(String option) throws Exception {
		Select dropdown = new Select(directBuyer);
		dropdown.selectByVisibleText(option);
		return new PCFPage();
	}
	
	public PCFPage selectDirectBuyerDropDown(DataTable reason, int row, int column) throws Exception {		
		List<List<String>> data = reason.raw();
		waitAndClickElement(directBuyer);
		clickOnTextFromDropdownList(directBuyer, data.get(row).get(column));
		
		return new PCFPage();
	}
	
	public PCFPage enterComments(String comment) throws Exception	{
		sendKeysToWebElement(comments, "PCF this is a test 123 !@#$%^&*()~`{}|[]:;<|>?,./ test");
		return new PCFPage();		
	}
	
	public PCFPage clickSave() throws Exception	{
		waitAndClickElement(saveButton);
		return new PCFPage();	
	}

	public String getSaveMessage()	{
		return driver.findElement((By) saveMessage).getText();
	}

	public String getFormSuccessMessage() { 
		  return driver.findElement((By) formSuccessMessage).getText();
		}

	@SuppressWarnings("unchecked")
	public void verifyPCFCleared() { 

	    Assert.assertFalse(((List<Map<String,String>>) contractDate).isEmpty());
	    Assert.assertFalse(((List<Map<String,String>>) closingDate).isEmpty());
	    Assert.assertFalse(((List<Map<String,String>>) sellerConcessions).isEmpty());
	    Assert.assertFalse(((List<Map<String,String>>) contractPrice).isEmpty());
	    Assert.assertFalse(((List<Map<String,String>>) comments).isEmpty());

	}
	
	@SuppressWarnings("unchecked")
	public void verifyPCFFilled() { 

	    Assert.assertTrue(((List<Map<String,String>>) contractDate).isEmpty());
	    Assert.assertTrue(((List<Map<String,String>>) closingDate).isEmpty());
	    Assert.assertTrue(((List<Map<String,String>>) sellerConcessions).isEmpty());
	    Assert.assertTrue(((List<Map<String,String>>) contractPrice).isEmpty());
	    Assert.assertTrue(((List<Map<String,String>>) comments).isEmpty());	
	}
}
	