package pageObjects;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.DataTable;
import utils.ExcelReader;

public class ICFPage extends BasePage {
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[1]/div[1]/select[1]")
	WebElement didYouSetListingAppointment;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[3]/div[1]/select[1]\r\n")
	WebElement reasonForNotSetting;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[4]/div[1]/div[1]/span[1]/input[1]")
	WebElement nextFollowUpDate;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[2]/div[1]/div[1]/span[1]/input[1]")
	WebElement listAppointmentDate;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[6]/div[1]/textarea[1]")
	WebElement comments;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[5]/label[1]/div[1]/input[1]")
	WebElement noFurtherFollowUpRequired;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[5]/div[1]/div[1]/div[1]/div[1]/p[1]")
	WebElement noFurtherFollowUPopUpMsg;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[5]/div[1]/div[1]/div[1]/div[2]/button[1]")
	WebElement noFurtherFollowUpPopUpButton;
	
	public ICFPage() throws IOException {
		super();
	}

	public ICFPage enterYesterdayDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
	
		String yesterday = testData.get(rowNumber).get("Yesterday");

		icfPage.getNextFollowUpDate(yesterday);
		icfPage.getListAppointmentDate(yesterday);
		
		Assert.assertNotNull(yesterday);
		
		return new ICFPage();
	}
	
	public ICFPage enterCurrentDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String currentdate = testData.get(rowNumber).get("CurrentDate");
		
		icfPage.getNextFollowUpDate(currentdate);
		icfPage.getListAppointmentDate(currentdate);
		
		Assert.assertNotNull(currentdate);
		
		return new ICFPage();
	}
	
	public ICFPage enterTomorrowDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String tomorrow = testData.get(rowNumber).get("Tomorrow");
		
		icfPage.getNextFollowUpDate(tomorrow);
		icfPage.getListAppointmentDate(tomorrow);
		
		Assert.assertNotNull(tomorrow);
		
		return new ICFPage();
	}
	
	public void getNextFollowUpDate(String date) throws Throwable  	{
		sendKeysToWebElement(nextFollowUpDate, date);
	}
	
	public void getListAppointmentDate(String date) throws Throwable  	{
		sendKeysToWebElement(listAppointmentDate, date);
	}
	
	public ICFPage clickOptionDropDown(String option) throws Exception {
		Select dropdown = new Select(didYouSetListingAppointment);
		dropdown.selectByVisibleText(option);
		return new ICFPage();
	}
	
	public ICFPage clickReasonDropDown(DataTable reason, int row, int column) throws Exception {		
		List<List<String>> data = reason.raw();
		waitAndClickElement(reasonForNotSetting);
		clickOnTextFromDropdownList(reasonForNotSetting, data.get(row).get(column));
		
		return new ICFPage();
	}
	
	public ICFPage enterComments(String comment) throws Exception	{
		sendKeysToWebElement(comments, "ICF this is a test 123 !@#$%^&*()~`{}|[]:;<|>?,./ test");
		return new ICFPage();		
	}
	
	public ICFPage checkNoFurtherFollowUpRequired() throws Exception	{
		Assert.assertFalse(noFurtherFollowUpRequired.isSelected());
		waitAndClickElement(noFurtherFollowUpRequired);
		Assert.assertTrue(noFurtherFollowUpRequired.isSelected());
		
		WaitUntilWebElementIsVisible(noFurtherFollowUPopUpMsg);
		Assert.assertEquals(noFurtherFollowUPopUpMsg, formsPage.getSaveMessage());
		
		return new ICFPage();	
	}

	public String getNoFurtherFollowUpMessage() {
		return driver.findElement((By) noFurtherFollowUPopUpMsg).getText();
		}

	public ICFPage closeNoFurtherFollowUpMessage() throws Throwable, IOException 	{
		
		waitAndClickElement(noFurtherFollowUpPopUpButton);
		return new ICFPage();
	}
}
	