package pageObjects;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import cucumber.api.DataTable;
import utils.ExcelReader;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
	
		public @FindBy(xpath = "//body/div[1]/div[1]/form[1]/div[1]/div[1]/input[1]") 
		WebElement textfield_EmailAddress;
		
		public @FindBy(xpath = "//body/div[1]/div[1]/form[1]/div[2]/div[1]/input[1]") 
		WebElement textfield_Password;
		
		public @FindBy(xpath = "//button[contains(text(),'Login')]") 
		WebElement button_Submit;
		
		public @FindBy(xpath = "//button[contains(text(),'Request a Reset Link')]") 
		WebElement button_ResetRequest;
		
		public @FindBy(xpath = "//a[contains(text(),'Forgot password?')]") 
		WebElement link_ForgotPassword;
		
		public @FindBy(xpath = "//a[contains(text(),'Back to login')]") 
		WebElement link_BackToLogin;

		public @FindBy(xpath = "//input[@id='search']") 
		WebElement searchfield_Impersonator;

		public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/nav[1]/div[2]/ul[1]/li[1]/div[1]") 
		WebElement select_Impersonator;
		
		public @FindBy(xpath = "//body/div[1]/div[1]/img[1]") 
		WebElement header_loginpage;
		
		public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]")
		WebElement errorMessage;
		
		public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]")
		WebElement loginHeaderMessage;
		
		public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]")
		WebElement forgotPasswordHeaderMessage;
		
		public @FindBy(xpath = "/html[1]/body[1]/div[3]/div[1]/p[1]")
		WebElement requestToastNofication;

		public LoginPage() throws IOException {
			super();
		}
		
		public LoginPage loginToThePortal() throws Throwable	{
		
			navigationPage.openAgentPortal();
			loginPage.getEmailAddress("IdealAgentQA@Gmail.com");
			loginPage.clickSubmitButton();
			loginPage.getPassword("Password1");
			loginPage.clickSubmitButton();
			navigationPage.confirmLogin();
			
			return new LoginPage();
			
		}
		
		public LoginPage getEmailLoginCredentials(String sheetName, Integer rowNumber) throws Throwable	{
			
			ExcelReader reader = new ExcelReader();
			List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\LoginCredentials.xlsx", sheetName);
			
			String emailAddress = testData.get(rowNumber).get("EmailAddress");
			loginPage.getEmailAddress(emailAddress);
			
			return new LoginPage();
		}
		
		public LoginPage getPasswordLoginCredentials(String sheetName, Integer rowNumber) throws Throwable	{
			
			ExcelReader reader = new ExcelReader();
			List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\LoginCredentials.xlsx", sheetName);
		
			String password = testData.get(rowNumber).get("PassWord");
			loginPage.getPassword(password);
			
			return new LoginPage();
		}
		
		public void getEmailAddress(String emailAddress) throws Throwable  	{
			sendKeysToWebElement(textfield_EmailAddress, emailAddress);
		}
		
		public void getPassword(String password) throws Throwable  {	
			sendKeysToWebElement(textfield_Password, password);
		}
		
		public void getImpersonator(String impersonator) throws Throwable  {	
			sendKeysToWebElement(searchfield_Impersonator, impersonator);
		}
		
		public LoginPage enterEmailAddress(DataTable emailAddress, int row, int column) throws Exception {
			List<List<String>> data = emailAddress.raw();
			sendKeysToWebElement(textfield_EmailAddress, data.get(row).get(column));
			return new LoginPage();
		}
		
		public LoginPage enterPassword(DataTable password, int row, int column) throws Exception {
			List<List<String>> data = password.raw();
			sendKeysToWebElement(textfield_Password, data.get(row).get(column));
			return new LoginPage();
		}
		
		public LoginPage clickSubmitButton() throws Exception {
			waitAndClickElement(button_Submit);
			return new LoginPage();
		}
		
		public LoginPage clickForgotPassword() throws Exception {
			waitAndClickElement(link_ForgotPassword);
			return new LoginPage();
		}
			
		public LoginPage clickRequestAResetButton() throws Exception {
			waitAndClickElement(button_ResetRequest);
			return new LoginPage();
		}
		
		public LoginPage clickBackToLogin() throws Exception {
			waitAndClickElement(link_BackToLogin);
			return new LoginPage();
		}
		
		public LoginPage enterImpersonatorName(DataTable impersonator, int row, int column) throws Exception {
			List<List<String>> data = impersonator.raw();
			waitAndClickElement(searchfield_Impersonator);
			sendKeysToWebElement(searchfield_Impersonator, data.get(row).get(column));
			return new LoginPage();
		}
		
		public LoginPage selectImpersonator() throws Exception {
			waitAndClickElement(select_Impersonator);
			return new LoginPage();
		}
	  
		public LoginPage verifyLogoDisplays() throws Exception {
			WaitUntilWebElementIsVisible(header_loginpage);
			return new LoginPage();
		}
		
		public String getErrorMessage() { 
		  return driver.findElement((By) errorMessage).getText();
		}
		
		public String getLoginHeaderMessage() { 
			  return driver.findElement((By) loginHeaderMessage).getText();
		}
		
		public String getForgotPasswordHeaderMessage() { 
			  return driver.findElement((By) forgotPasswordHeaderMessage).getText();
		}
		
		public String getToastNotification() { 
			  return driver.findElement((By) requestToastNofication).getText();
		}

  }
	 
