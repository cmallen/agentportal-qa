package pageObjects;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.DataTable;
import utils.ExcelReader;

public class LAFPage extends BasePage {
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[1]/div[1]/select[1]")
	WebElement didYouSecureListing;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[3]/div[1]/div[1]/select[1]")
	WebElement reasonForNotSecuring;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[3]/div[2]/div[1]/div[1]/div[1]/span[1]/input[1]")
	WebElement nextFollowUpDate;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[2]/div[1]/div[1]/span[1]/input[1]")
	WebElement listApptSignedDate;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[2]/div[2]/div[1]/span[1]/input[1]")
	WebElement activeListingDate;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[3]/div[3]/label[1]/div[1]/input[1]")
	WebElement noFurtherFollowUpRequired;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[3]/div[3]/div[1]/div[1]/div[1]/div[1]/p[1]")
	WebElement noFurtherFollowUPopUpMsg;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[3]/div[3]/div[1]/div[1]/div[1]/div[2]/button[1]")
	WebElement noFurtherFollowUpPopUpButton;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[2]/div[3]/div[1]/input[1]")
	WebElement listPrice;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[4]/div[1]/textarea[1]")
	WebElement comments;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[5]/button[1]")
	WebElement saveButton;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[1]")
	WebElement saveMessage;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[1]")
	WebElement 	formSuccessMessage;
	
	public LAFPage() throws IOException {
		super();
	}

	public LAFPage clickOptionDropDown(String option) throws Exception {
		Select dropdown = new Select(didYouSecureListing);
		dropdown.selectByVisibleText(option);
		return new LAFPage();
	}
	
	public LAFPage clickReasonDropDown(DataTable reason, int row, int column) throws Exception {		
		List<List<String>> data = reason.raw();
		waitAndClickElement(reasonForNotSecuring);
		clickOnTextFromDropdownList(reasonForNotSecuring, data.get(row).get(column));
		
		return new LAFPage();
	}
	


	public LAFPage enterYesterdayDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String yesterday = testData.get(rowNumber).get("Yesterday");
		
		lafPage.getNextFollowUpDate(yesterday);
		lafPage.getListAgreementSignedDate(yesterday);
		
		Assert.assertNotNull(yesterday);
	
		return new LAFPage();
	}
	
	public LAFPage enterCurrentDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String currentdate = testData.get(rowNumber).get("CurrentDate");
		
		lafPage.getNextFollowUpDate(currentdate);
		lafPage.getListAgreementSignedDate(currentdate);
		
		Assert.assertNotNull(currentdate);
		
		return new LAFPage();
	}
	
	public LAFPage enterTomorrowDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String tomorrow = testData.get(rowNumber).get("Tomorrow");
		
		lafPage.getNextFollowUpDate(tomorrow);
		lafPage.getListAgreementSignedDate(tomorrow);
		
		Assert.assertNotNull(tomorrow);
		
		return new LAFPage();
	}
	
	public void getNextFollowUpDate(String date) throws Throwable  	{
		sendKeysToWebElement(nextFollowUpDate, date);
	}
	
	public void getListAgreementSignedDate(String date) throws Throwable  	{
		sendKeysToWebElement(listApptSignedDate, date);
	}
	
	public LAFPage enterComments(String comment) throws Exception	{
		sendKeysToWebElement(comments, "this is a test 123 !@#$%^&*()~`{}|[]:;<|>?,./ test");
		return new LAFPage();		
	}
	
	public LAFPage clickSave() throws Exception	{
		waitAndClickElement(saveButton);
		return new LAFPage();	
	}
	
	public String getFormSuccessMessage() { 
		  return driver.findElement((By) formSuccessMessage).getText();
		}

	public String getSaveMessage()	{
		return driver.findElement((By) saveMessage).getText();
	}
	
	public LAFPage checkNoFurtherFollowUpRequired() throws Exception	{
		Assert.assertFalse(noFurtherFollowUpRequired.isSelected());
		waitAndClickElement(noFurtherFollowUpRequired);
		Assert.assertTrue(noFurtherFollowUpRequired.isSelected());
		
		WaitUntilWebElementIsVisible(noFurtherFollowUPopUpMsg);
		Assert.assertEquals(noFurtherFollowUPopUpMsg, getSaveMessage());
		
		return new LAFPage();	
	}

	public String getNoFurtherFollowUpMessage() {
		return driver.findElement((By) noFurtherFollowUPopUpMsg).getText();
		}

	public LAFPage closeNoFurtherFollowUpMessage() throws Throwable, IOException 	{
		
		waitAndClickElement(noFurtherFollowUpPopUpButton);
		return new LAFPage();
	}
}
	