package pageObjects;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.DataTable;
import utils.ExcelReader;

public class SPFPage extends BasePage {
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/select[1]")
	WebElement didClosingOccurOnScheduledDate;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[1]/div[1]/input[1]")
	WebElement uploadALTA;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[2]/div[1]/div[1]/span[1]/input[1]")
	WebElement finalSoldDate;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[3]/div[1]/button[1]")
	WebElement contractFellThrough;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/form[1]/div[3]/button[1]")
	WebElement saveButton;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[1]")
	WebElement saveMessage;
	
	public @FindBy(xpath = "/html[1]/body[1]/div[1]/div[3]/main[1]/article[1]/div[2]/div[2]/div[1]/div[1]/div[1]")
	WebElement 	formSuccessMessage;
	
	public SPFPage() throws IOException {
		super();
	}
	
	public SPFPage enterYesterdayDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String yesterday = testData.get(rowNumber).get("Yesterday");
		
		spfPage.getFinalSoldDate(yesterday);
		
		Assert.assertNotNull(yesterday);
	
		return new SPFPage();
	}
	
	public SPFPage enterCurrentDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String currentdate = testData.get(rowNumber).get("CurrentDate");
		
		spfPage.getFinalSoldDate(currentdate);
		
		Assert.assertNotNull(currentdate);
		
		return new SPFPage();
	}
	
	public SPFPage enterTomorrowDate(String sheetName, Integer rowNumber) throws Throwable {
		
		ExcelReader reader = new ExcelReader();
		List<Map<String,String>> testData = reader.getData("C:\\Automation\\eclipse-workspace\\agent-portal-cucumberframework\\CucumberFramework\\src\\main\\java\\testData\\DatePicker.xlsx", sheetName);
		
		String tomorrow = testData.get(rowNumber).get("Tomorrow");
		
		spfPage.getFinalSoldDate(tomorrow);
		
		Assert.assertNotNull(tomorrow);
		
		return new SPFPage();
	}
	
	public void getFinalSoldDate(String date) throws Throwable  	{
		sendKeysToWebElement(finalSoldDate, date);
	}
	
	public SPFPage clickOptionDropDown(String option) throws Exception {
		Select dropdown = new Select(didClosingOccurOnScheduledDate);
		dropdown.selectByVisibleText(option);
		return new SPFPage();
	}
	
	public SPFPage selectOptionDropDown(DataTable reason, int row, int column) throws Exception {		
		List<List<String>> data = reason.raw();
		waitAndClickElement(didClosingOccurOnScheduledDate);
		clickOnTextFromDropdownList(didClosingOccurOnScheduledDate, data.get(row).get(column));
		
		return new SPFPage();
	}
	
	public SPFPage clickSave() throws Exception	{
		waitAndClickElement(saveButton);
		return new SPFPage();	
	}

	public String getSaveMessage()	{
		return driver.findElement((By) saveMessage).getText();
	}

	public String getFormSuccessMessage() { 
		  return driver.findElement((By) formSuccessMessage).getText();
		}

}
	