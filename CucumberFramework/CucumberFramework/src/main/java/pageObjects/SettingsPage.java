package pageObjects;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SettingsPage extends BasePage {
	
	/* Personal Information */
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[1]/div[1]/div[2]/div[1]/span[1]/div[1]/div[1]/input[1]") 
	WebElement textfield_BrokerageName;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[1]/div[1]/div[3]/button[1]") 
	WebElement button_SavePersonalInfo;
	
	/* Contact Information */
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[2]/div[1]/div[2]/div[1]/span[1]/div[1]/div[1]/input[1]") 
	WebElement textfield_AgentEmailAddress;
	
	public @FindBy(xpath = "//button[contains(text(),'Send code')]") 
	WebElement button_SendCode;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[3]/div[1]/div[1]/div[1]/span[1]/div[1]/input[1]") 
	WebElement textfield_AgentPhoneNumber;
	
	public @FindBy(xpath = "//button[contains(text(),'Call')]") 
	WebElement button_Call;
	
	public @FindBy(xpath = "//button[contains(text(),'Text')]") 
	WebElement button_Text;
	
	/* Address Information */
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[4]/div[1]/div[2]/div[1]/select[1]") 
	WebElement dropdown_Country;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[4]/div[1]/div[3]/div[1]/select[1]") 
	WebElement dropdown_State;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[4]/div[1]/div[4]/div[1]/select[1]") 
	WebElement dropdown_City;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[4]/div[1]/div[5]/div[1]/span[1]/div[1]/div[1]/input[1]") 
	WebElement textfield_Address;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[4]/div[1]/div[6]/div[1]/div[1]/input[1]") 
	WebElement textfield_Zip;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[4]/div[1]/div[7]/button[1]") 
	WebElement button_SaveAddressInfo;
	
	/* Assistant Information */
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[5]/div[1]/div[2]/div[1]/span[1]/div[1]/div[1]/input[1]") 
	WebElement textfield_AssistantName;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[5]/div[1]/div[3]/div[1]/span[1]/div[1]/div[1]/input[1]") 
	WebElement textfield_AssistantEmail;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[5]/div[1]/div[4]/span[1]/div[1]/input[1]") 
	WebElement textfield_AssistantPhoneNumber;
	
	public @FindBy(xpath = "//body/div[1]/div[3]/div[1]/form[5]/div[1]/div[5]/button[1]") 
	WebElement button_SaveAssistantInfo;
	
	public SettingsPage() throws IOException {
		super();
	}
	
	/* Personal Information */
	
	public SettingsPage enterBrokerageName () throws Exception {
		WaitUntilWebElementIsVisible(textfield_BrokerageName);
		return new SettingsPage();
	}
	
	public SettingsPage clickSavePersonalInfo () throws Exception {
		
		return new SettingsPage();
	}
	
	public SettingsPage verifyMessagePersonalInfo () throws Exception {
		
		return new SettingsPage();
	}
	
	
	

}
