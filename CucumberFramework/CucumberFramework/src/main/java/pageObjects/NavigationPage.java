package pageObjects;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigationPage extends BasePage {
	
		public @FindBy(xpath = "//div[contains(text(),'Change User')]") WebElement nav_ChangeUser;
		public @FindBy(xpath = "//h3[contains(text(),'No user selected')]") WebElement header_ChangeUser;
	
		public @FindBy(xpath = "//p[contains(text(),'Dashboard')]") WebElement nav_Dashboard;
		public @FindBy(xpath = "//p[contains(text(),'Converted Appointments')]") WebElement header_Dashboard;
	
		public @FindBy(xpath = "//p[contains(text(),'My Referrals')]") WebElement nav_MyReferrals;
		public @FindBy(xpath = "//label[contains(text(),'Sellers')]") WebElement header_MyReferrals;

		public @FindBy(xpath = "//p[contains(text(),'Settings')]") WebElement nav_Settings;
		public @FindBy(xpath = "//h1[contains(text(),'Agent Bio')]") WebElement header_Settings;

		public @FindBy(xpath = "//body/div[1]/div[1]/div[2]/div[2]/nav[1]/div[3]/button[1]") WebElement nav_LogOut;
		public @FindBy(xpath = "//div[contains(text(),'This application is for authorized partner agents ')]") WebElement header_LogOut;
		
		public @FindBy(xpath = "//body/div[1]/div[1]/img[1]") WebElement confirm_logout;
		public @FindBy(xpath = "//body/div[1]/div[2]/div[1]/a[1]/img[1]") WebElement confirm_login;
		
		public @FindBy(xpath = "") WebElement select_Seller;
		public @FindBy(xpath = "") WebElement select_Buyer;
			
		public NavigationPage() throws IOException {
			super();
		}
		
		public NavigationPage openAgentPortal() throws IOException {
			
			getDriver().get("https://dev-partner.idealagent.com/");
			return new NavigationPage(); 
		}
		
		public NavigationPage confirmLogin() throws Exception {
			WaitUntilWebElementIsVisible(confirm_login);
			return new NavigationPage();
		}
		
		public NavigationPage confirmLogout() throws Exception {
			WaitUntilWebElementIsVisible(confirm_logout);
			return new NavigationPage();
			}

		public NavigationPage navigateToChangeUser() throws Exception {
			waitAndClickElement(nav_ChangeUser);
			WaitUntilWebElementIsVisible(header_ChangeUser);
			return new NavigationPage();
		}
		
		public NavigationPage navigateToDashboard() throws Exception {
			waitAndClickElement(nav_Dashboard);
			WaitUntilWebElementIsVisible(header_Dashboard);
			return new NavigationPage();
		}
			
		public NavigationPage navigateToMyReferrals() throws Exception {
			waitAndClickElement(nav_MyReferrals);
			WaitUntilWebElementIsVisible(header_MyReferrals);
			return new NavigationPage();
		}
		
		public NavigationPage navigateToSettings() throws Exception {
			waitAndClickElement(nav_Settings);
			WaitUntilWebElementIsVisible(header_Settings);
			return new NavigationPage();
		}
		
		public NavigationPage navigateToLogOut() throws Exception {
			waitAndClickElement(nav_LogOut);
			WaitUntilWebElementIsVisible(header_LogOut);
			return new NavigationPage();
		}
		
		public NavigationPage selectSeller() throws Exception {
			
			waitAndClickElement(select_Seller);
			return new NavigationPage();	
		}
		
		public NavigationPage selectBuyer() throws Exception {
			
			waitAndClickElement(select_Buyer);
			return new NavigationPage();	
		}
		
  }